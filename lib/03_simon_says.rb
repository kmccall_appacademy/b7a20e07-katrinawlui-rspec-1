def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string, repeats=2)
  ([string] * repeats).join(" ")
end

def start_of_word(word, n)
  result = ""
  0.upto(n - 1) { |idx| result << word[idx] }
  result
end

def first_word(string)
  array = string.split
  array.first
end

def titleize(string)
  result = []

  array = string.split
  array.each_with_index do |word, idx|
    if word.length <= 4 && idx != 0 && idx != array.length - 1
      result << word
    else
      result << word.capitalize
    end
  end

  result.join(" ")
end
