def translate(string)
  array = string.split
  array.map! { |word| translate_word(word) }
  array.join(" ")
end

def translate_word(word)
  first_vowel_idx = word.index(/[aeiou]/)
  if first_vowel_idx == 0
    return word << "ay"
  else
    first_consonants = word[0...first_vowel_idx]
    if word[first_vowel_idx] == "u" && word[first_vowel_idx - 1] == "q"
      return word[(first_vowel_idx + 1)..-1] << first_consonants << "uay"
    else
      return word[first_vowel_idx..-1] << first_consonants << "ay"
    end
  end
end
