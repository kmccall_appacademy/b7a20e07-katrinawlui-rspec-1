def ftoc(temp)
  result = (temp - 32) / 1.8
  result.ceil
end

def ctof(temp)
  result = (temp * 1.8) + 32
  result
end
