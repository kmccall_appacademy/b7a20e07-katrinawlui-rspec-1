def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(array)
  return 0 if array.empty?
  array.reduce(:+)
end

def multiply(*numbers)
  numbers.reduce(:*)
end

def power(num1, num2)
  num1**num2
end

def factorial(num)
  return 0 if num.zero?
  result = []
  num.downto(1) { |factor| result << factor }
  result.reduce(:*)
end
